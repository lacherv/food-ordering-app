import { Link } from "react-router-dom";
import logo from "../assets/logo.png"
import "../styles/navbar.css";
import ReorderIcon from "@mui/icons-material/Reorder";
import { useState } from "react";

const Navbar = () => {

    const [showLinks, setShowLinks] = useState(false);
    const handleCliks = () => {
        setShowLinks(!showLinks);
    }

    return (
        <div className="navbar">
            <div className="leftSide" id={showLinks ? "open" : "close"}>
                <img src={logo} alt="logo" />
                <div className="hiddenLinks">
                    <div className="right-side">
                        <Link to="/">Home</Link>
                        <Link to="/menu">Menu</Link>
                        <Link to="/about">About</Link>
                        <Link to="/contact">Contact</Link>
                    </div>
                </div>
            </div>
            <div className="rightSide">
                <Link to="/">Home</Link>
                <Link to="/menu">Menu</Link>
                <Link to="/about">About</Link>
                <Link to="/contact">Contact</Link>
                <button onClick={handleCliks}>
                    <ReorderIcon />
                </button>
            </div>
        </div>
    );
}

export default Navbar;