import '../../styles/menu.css';
import { MenuList } from '../../helpers/MenuList';
import MenuItem from '../MenuItem';

const Menu = () => {
    return (
        <div className="menu">
            <h1 className="menuTitle">Our Menu</h1>
            <div className="menuList">
                {MenuList && MenuList.map((menuItem, index) => {
                    return (
                        <MenuItem
                            key={index}
                            image={menuItem.image}
                            name={menuItem.name}
                            price={menuItem.price}
                        />
                    )
                })}
            </div>
        </div>
    );
}

export default Menu;