import './App.css';
import Navbar from './components/Navbar';
import { Routes, Route } from 'react-router-dom';
import Home from './components/pages/Home';
import Footer from './components/Footer';
import Menu from './components/pages/Menu';
import Contact from './components/pages/Contact';
import About from './components/pages/About';

const App = () => {
  return (
    <div className="App">
        <Navbar />
        <Routes>
          {/* React router v6 */}
          <Route path="/" element={<Home />} />
          <Route path="/menu" element={<Menu />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/about" element={<About />} />
        </Routes>
      <Footer />
    </div>
  );
}

export default App;